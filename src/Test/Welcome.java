import java.util.Scanner;

class AtmSimulator {

    static Scanner sc1 = new Scanner(System.in);
    static double accountBalance = 5000.25;
    static int pin;
    // for select language
    static {
        System.out.println("SELECT LANGUAGE");
        System.out.println("1:ENGLISH\t2:MARATHI");
        int choice = sc1.nextInt();
        if (choice == 1) {
            System.out.println("SELECTED ENGLISH");
        } else if (choice == 2) {
            System.out.println("SELECTED MARATHI");
        } else {
            System.out.println("INVALID CHOICE");
            System.exit(0);
        }
    }
    // for enter pin
    static {
        System.out.println("ENTER PIN");
        pin = sc1.nextInt();
    }
    // main method started

    public static void main(String[] args) {
        if (pin == 9999) {
            System.out.println("1:WITHDRAW\n2:CHECK BALANCE\n3:CHANGE PIN");
            int choice = sc1.nextInt();
            if (choice == 1) {
                System.out.println("ENTER AMOUNT");
                double amt = sc1.nextDouble();
                withdraw(amt);
            } else if (choice == 2) {
                checkBalance();
            } else if (choice == 3) {
                System.out.println("Enter New Pin");
                int newPin = sc1.nextInt();
                changePin(newPin);
            } else {
                System.out.println("Invalid Choise");
            }
        } else {
            System.out.println("Invalid Pin");
        }
    }

    // main method ended
    static void withdraw(double amt) {
        if (amt <= accountBalance) {
            accountBalance -= amt;
            System.out.println(amt + " RS Debited From Account");
        } else {
            System.out.println("Insufficient Balance");
        }
    }

    static void checkBalance() {
        System.out.println("Account Balance : " + accountBalance);
    }

    static void changePin(int newPin) {
        pin = newPin;
        System.out.println("Pin Changed");
    }
}
